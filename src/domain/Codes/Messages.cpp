//
// Created by Jarvis on 16/05/2021.
//

#include "Messages.h"

#include <utility>

Messages::Messages(int code, std::string message) : code(code), message(std::move(message)){}
