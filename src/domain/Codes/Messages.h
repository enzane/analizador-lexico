#ifndef ANALIZADORLEXICO_MESSAGES_H
#define ANALIZADORLEXICO_MESSAGES_H


#include <string>

class Messages {
    int code;
    std::string message;
    Messages(int code, std::string message);
};


#endif //ANALIZADORLEXICO_MESSAGES_H
