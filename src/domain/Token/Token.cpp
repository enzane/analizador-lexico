#include <iostream>
#include "Token.h"
#include "../StatesMachine/StateMachine.h"

#include <utility>
Token::Token(int tokenNumber, Lexeme lexeme, Grammeme grammeme) : tokenNumber(tokenNumber), lexeme(std::move(lexeme)),
                                                                  grammeme(std::move(grammeme)) {};
int Token::number = 0;
std::string Token::display() const {
    std::string token = std::to_string(this->number) + " ( " + this->lexeme.value + " " + this->grammeme.getValue() + " )";
    return token;
}

Token Token::createToken(Lexeme lexeme) {
    Grammeme grammeme = Grammeme::getGrammeme(lexeme);
    return Token(Token::number++, lexeme, grammeme);
}

std::vector<Token> Token::createTokens(const std::string &lexicElements) {
    std::vector<Token> tokens;
    StateMachine stateMachine = StateMachine(lexicElements[0]);;
    for (int i = 1; i < lexicElements.length() + 1; ++i) {
        stateMachine.nextStep(lexicElements[i]);
    }
    for (auto& element:stateMachine.elements) {
        Token token = createToken(element);
        tokens.push_back(token);
    }
    return tokens;
}



