#ifndef TOKEN_H
#define TOKEN_H

#include <string>
#include "../Grammeme/Grammeme.h"
#include "../Lexeme/Lexeme.h"
#include <vector>

class Token {
public:
    std::string display() const;
    static Token createToken(Lexeme lexeme);
    static std::vector<Token> createTokens(const std::string& lexicElements);
private:
    static int number;
    int tokenNumber;
    Grammeme grammeme;
    Lexeme lexeme;
    Token(int tokenNumber, Lexeme  lexeme, Grammeme grammeme);
};


#endif //ANALIZADORLEXICO_TOKEN_H
