#ifndef LEXEME_H
#define LEXEME_H


#include <string>

class Lexeme {
    public:
        std::string value;
        int code;

    Lexeme(std::string value, int code);
};


#endif
