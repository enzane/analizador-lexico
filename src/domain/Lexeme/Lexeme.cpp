#include "Lexeme.h"

#include <utility>

Lexeme::Lexeme(std::string value, int code) : value(std::move(value)), code(code) {}
