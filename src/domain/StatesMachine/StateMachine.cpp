#include <iostream>
#include "StateMachine.h"
#include "ConcreteMachines/Initial.h"
#include "ConcreteMachines/Identifier.h"
#include "Clasification.h"
#include "ConcreteMachines/Numeric.h"
#include "ConcreteMachines/Character.h"
#include "ConcreteMachines/String.h"
#include "ConcreteMachines/Plus.h"
#include "ConcreteMachines/Minus.h"
#include "ConcreteMachines/Asterisk.h"
#include "ConcreteMachines/FrontSlash.h"

StateMachine::StateMachine(char character) {
    this->actualMachine = new Initial();
    this->actualMachine->actualCharacter = character;
    this->actualCode = this->actualMachine->evaluate();
    addElement(character);
    transition(character);
}

void StateMachine::transition(char character) {
    changeMachine(character);
    std::cout << this->actualCode << character << " ";

    if(actualCode >= 100 && actualCode < 200) {
        actualMachine = new Initial();
        Lexeme lexeme(this->element, actualCode);
        this->elements.push_back(lexeme);
        this->element = "";
        this->nextStep(character);
    }
}

void StateMachine::changeMachine(char character) {
    if(actualCode == 0) {
        actualMachine = new Initial();
        actualMachine->actualCharacter = character;
    }
    if(actualCode == 1) {
        actualMachine = new Identifier();
        actualMachine->actualCharacter = character;
    }
    if(actualCode == 3) {
        actualMachine = new Numeric();
        actualMachine->actualCharacter = character;
    }
    if(actualCode == 9) {
        actualMachine = new Character();
        actualMachine->actualCharacter = character;
    }
    if(actualCode == 12) {
        actualMachine = new String();
        actualMachine->actualCharacter = character;
    }
    if(actualCode == 14) {
        actualMachine = new Plus();
        actualMachine->actualCharacter = character;
    }
    if(actualCode == 16) {
        actualMachine = new Minus();
        actualMachine->actualCharacter = character;
    }
    if(actualCode == 18) {
        actualMachine = new Asterisk();
        actualMachine->actualCharacter = character;
    }
    if(actualCode == 20) {
        actualMachine = new FrontSlash();
        actualMachine->actualCharacter = character;
    }
    /*if(actualCode == 9) {
        actualMachine = new Numeric();
        actualMachine->actualCharacter = character;
    }
    if(actualCode == 10) {
        actualMachine = new Numeric();
        actualMachine->actualCharacter = character;
    }
    if(actualCode == 11) {
        actualMachine = new Numeric();
        actualMachine->actualCharacter = character;
    }
    if(actualCode == 12) {
        actualMachine = new Numeric();
        actualMachine->actualCharacter = character;
    }
    if(actualCode == 13) {
        actualMachine = new Numeric();
        actualMachine->actualCharacter = character;
    }
    if(actualCode == 14) {
        actualMachine = new Numeric();
        actualMachine->actualCharacter = character;
    }
    if(actualCode == 15) {
        actualMachine = new Numeric();
        actualMachine->actualCharacter = character;
    }
    if(actualCode == 16) {
        actualMachine = new Numeric();
        actualMachine->actualCharacter = character;
    }*/
}

void StateMachine::addElement(char character) {
    if(this->actualCode > 0 && this->actualCode < 100) {
        this->element.push_back(character);
    }
    if(this->actualCode >= 500) {
        this->actualMachine = new Initial();
        this->element = "";
    }
}

void StateMachine::nextStep(char nextChar) {
    this->actualMachine->actualCharacter = nextChar;
    this->actualCode = this->actualMachine->evaluate();
    addElement(nextChar);
    transition(nextChar);
}
