#include <iostream>
#include "Identifier.h"
#include "../Clasification.h"

Identifier::Identifier() {
    this->state = "q1";
}

int Identifier::evaluate() {
    if(this->state == "q1") {
        if(Clasification::isUnderScore(this->actualCharacter)) {
            this->state = "q2";
            return 2;
        }
        if(Clasification::isLetter(this->actualCharacter) || Clasification::isNumber(this->actualCharacter)) {
            return 1;
        }

        return 100;
    }
    if(this->state == "q2") {
        if(Clasification::isDelimiter(this->actualCharacter)) {
            return 501;
        }
        if(Clasification::isUnderScore(this->actualCharacter)) {
            return 2;
        }
        if(Clasification::isLetter(this->actualCharacter)
        || Clasification::isNumber(this->actualCharacter) ) {
            this->state = "q1";
            return 1;
        }
    }
    return 501;
}
