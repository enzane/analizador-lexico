//
// Created by Jarvis on 19/05/2021.
//

#include "String.h"
#include "../Clasification.h"

String::String() {
    this->state = "q12";
}

int String::evaluate() {
    if(this->state == "q12") {
        if(Clasification::isDoubleQuote(this->actualCharacter)) {
            this->state = "q13";
            return 13;
        }
        return 12;
    }
    if(this->state == "q13") {
        return 105;
    }
}
