#include "Character.h"
#include "../Clasification.h"

Character::Character() {
    this->state = "q9";
}

int Character::evaluate() {
    if(this->state == "q9") {
        if(!Clasification::isSingleQuote(this->actualCharacter)) {
            this->state = "q10";
            return 10;
        }
        return 508;
    }
    if(this->state == "q10") {
        if(Clasification::isSingleQuote(this->actualCharacter)) {
            this->state = "q11";
            return 11;
        }
        return 509;
    }
    if(this->state == "q11") {
        if(Clasification::isDelimiter(this->actualCharacter)) {
            return 104;
        }
        return 510;
    }

    return 501;
}