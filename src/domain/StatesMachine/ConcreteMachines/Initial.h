#ifndef ANALIZADORLEXICO_INITIAL_H
#define ANALIZADORLEXICO_INITIAL_H


#include "../AbstractStateMachine.h"

class Initial: public AbstractStateMachine{
public:
    explicit Initial();
    std::string state = "q0";
    int evaluate() override;
};


#endif //ANALIZADORLEXICO_INITIAL_H
