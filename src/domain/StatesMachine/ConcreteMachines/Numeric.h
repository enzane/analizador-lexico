#ifndef ANALIZADORLEXICO_NUMERIC_H
#define ANALIZADORLEXICO_NUMERIC_H
#include <string>
#include "../AbstractStateMachine.h"
#include "../../Codes/Messages.h"
#include "../../Codes/Errors.h"

class Numeric: public AbstractStateMachine {
public:
    Numeric();
    int evaluate() override;
};


#endif //ANALIZADORLEXICO_NUMERIC_H
