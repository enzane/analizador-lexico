#include "Minus.h"
#include "../Clasification.h"

Minus::Minus() {
    this->state = "q16";
}

int Minus::evaluate() {
    if(this->state == "q16") {
        if(Clasification::isMinus(this->actualCharacter)) {
            return 106;
        }
        if(Clasification::isEquals(this->actualCharacter)) {
            this->state = "q17";
            return 17;
        }
        return 106;
    }
    if(this->state == "q17") {
        return 107;
    }
}
