#include "Plus.h"
#include "../Clasification.h"

Plus::Plus() {
    this->state = "q14";
}

int Plus::evaluate() {
    if(this->state == "q14") {
        if(Clasification::isPlus(this->actualCharacter)) {
            return 106;
        }
        if(Clasification::isEquals(this->actualCharacter)) {
            this->state = "q15";
            return 15;
        }
        return 106;
    }
    if(this->state == "q15") {
        return 107;
    }
}

