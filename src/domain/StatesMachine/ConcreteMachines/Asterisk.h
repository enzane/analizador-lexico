#ifndef ANALIZADORLEXICO_ASTERISK_H
#define ANALIZADORLEXICO_ASTERISK_H


#include "../AbstractStateMachine.h"

class Asterisk: public AbstractStateMachine {
public:
    Asterisk();
    int evaluate() override;

};


#endif