#include "FrontSlash.h"
#include "../Clasification.h"

FrontSlash::FrontSlash() {
    this->state = "q20";
}

int FrontSlash::evaluate() {
    if(this->state == "q20") {
        if(Clasification::isEquals(this->actualCharacter)) {
            this->state = "q21";
            return 21;
        }
        if(Clasification::isAsterisk(this->actualCharacter)) {
            this->state = "q22";
            return 22;
        }
        return 106;
    }
    if(this->state == "q21") {
        return 107;
    }
    if(this->state == "q22") {
        if(Clasification::isAsterisk(this->actualCharacter)) {
            this->state = "q23";
            return 23;
        }
        return 22;
    }
    if(this->state == "q23") {
        if(Clasification::isFrontSlash(this->actualCharacter)) {
            this->state = "q24";
            return 24;
        }
        this->state = "q22";
        return 22;
    }
    if(this->state == "q24") {
        return 108;
    }
}
