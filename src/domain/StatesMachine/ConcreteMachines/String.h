#ifndef ANALIZADORLEXICO_STRING_H
#define ANALIZADORLEXICO_STRING_H


#include "../AbstractStateMachine.h"

class String: public AbstractStateMachine {
public:
    String();
    int evaluate() override;
};


#endif