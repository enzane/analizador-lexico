#include "Clasification.h"
#include <iostream>

int Clasification::getAscii(char character) {
    return (int) character;
}
bool Clasification::isLetter(char character) {
    int code = getAscii(character);
    if( code >= 97 && code <= 122){
        return true;
    }
    if (code >= 65 && code <= 90) {
        return true;
    }

    return false;
}
bool Clasification::isNumber(char code) {
    if(getAscii(code) >= 48 && getAscii(code) <= 57) {
        return true;
    }
    return false;
}
bool Clasification::isUnderScore(char code) {
    if(getAscii(code) == 95) {
        return true;
    }
    return false;
}
bool Clasification::isSingleQuote(char code) {
    return (int) code == 39;
}
bool Clasification::isDoubleQuote(char code) {
    return code == '"';
}
bool Clasification::isLogicSymbol(char code) {
    if(getAscii(code) == (int) '|' || getAscii(code) == (int) '&') {
        return true;
    }
    return false;
}
bool Clasification::isPuncSymbol(char code) {
    if(code == ';' || code == '.' || code == ',') {
        return true;
    }
    return false;
}
bool Clasification::isAgrupSymbol(char code) {
    if(code == '[' || code == ']' || code == '(' || code == ')') {
        return true;
    }
    return false;
}
bool Clasification::isCommentSymbol(char code) {
    return code == '#';
}
bool Clasification::isDelimiter(char code) {
    if(getAscii(code) == 32 || getAscii(code) == 9 ||getAscii(code) == 10  || getAscii(code) == 0) {
        return true;
    }
    return false;
}
bool Clasification::isFrontSlash(char code) {
    return code == '/';
}
bool Clasification::isAdmiration(char code) {
    return code == '!';
}
bool Clasification::isEquals(char code) {
    return code == '=';
}
bool Clasification::isMinus(char code) {
    return code == '-';
}
bool Clasification::isPlus(char code) {
    return code == '+';
}
bool Clasification::isAsterisk(char code) {
    return code == '*';
}
bool Clasification::isPercentaje(char code) {
    return code == '%';
}
