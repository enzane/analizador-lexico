#include <iostream>
#include "domain/Lexeme/Lexeme.h"
#include "domain/Grammeme/Grammeme.h"
#include "domain/Token/Token.h"
#include <vector>
int main() {
    //std::vector<Token> tokens = Token::createTokens("a4bc_4 32 32.3 32.3e+4 \'fd\'");
    std::vector<Token> tokens = Token::createTokens("/=A**A*/cxz");
    for (auto &&token:tokens) {
        std::cout << "\n" << token.display() << std::endl;
    }
    return 0;
}

bool returnTrue() {
    return true;
}