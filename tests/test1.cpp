#include "gtest/gtest.h"
#include "../src/domain/Lexeme/Lexeme.h"
#include "../src/domain/Grammeme/Grammeme.h"
#include "../src/domain/Token/Token.h"

TEST(GenerarTokens, ImprimirCorrectamenteElToken) {

    ASSERT_STREQ("0 ( class, Palabras Reservadas )", "0 ( class, Palabras Reservadas )");
}

TEST(Grammeme, GenerarGramema) {
    Lexeme lexeme("class");
    ASSERT_STREQ("class", "class");
}
